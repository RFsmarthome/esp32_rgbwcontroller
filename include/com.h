#ifndef _COM_H
#define _COM_H

#include <Arduino.h>
#include <freertos/queue.h>

struct rgbw_t {
	uint8_t channel;
	uint8_t r, g, b, w;
};

class Com {
public:
	Com();
	virtual ~Com();

	bool send(struct rgbw_t *d, TickType_t wait);
	bool receive(struct rgbw_t *d, TickType_t wait);

private:
	QueueHandle_t m_queue;
};

#endif
