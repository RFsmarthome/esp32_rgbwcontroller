#ifndef _MQTT_H
#define _MQTT_H

#include "com.h"
#include "statled.h"

void mqttSetup(String &hostname, String &server, String &port, String &basePath, Com *com, StatLed *statLed);

bool mqttSendRGB(uint8_t channel, uint8_t r, uint8_t g, uint8_t b);
bool mqttSendRGBW(uint8_t channel, uint8_t r, uint8_t g, uint8_t b, uint8_t w);
bool mqttSendTemp(uint8_t channel, float temp);
bool mqttSendHumidity(uint8_t channel, float hum);
bool mqttSendPressure(uint8_t channel, float press);

#endif
