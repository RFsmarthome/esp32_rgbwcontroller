#ifndef _STATLED_H
#define _STATLED_H

#include <driver/gpio.h>

class StatLed {
public:
	StatLed(gpio_num_t gpio);
	void init();
	void on();
	void off();
	void set(bool b);

private:
	gpio_num_t m_gpio;
};

#endif
