#include <Arduino.h>
#include <Adafruit_BME280.h>
#include <SerialDebug.h>

#include <freertos/task.h>

#include "mqtt.h"

Adafruit_BME280 bme280;
Adafruit_Sensor *bme280Humidity = bme280.getHumiditySensor();
Adafruit_Sensor *bme280Pressure = bme280.getPressureSensor();
Adafruit_Sensor *bme280Temp = bme280.getTemperatureSensor();

void bme280ReadSensors()
{
    sensors_event_t evHum, evPress, evTemp;

    bme280Humidity->getEvent(&evHum);
    bme280Pressure->getEvent(&evPress);
    bme280Temp->getEvent(&evTemp);

    mqttSendHumidity(1, evHum.relative_humidity);
    mqttSendPressure(1, evPress.pressure);
    mqttSendTemp(1, evTemp.temperature);
}

void bme280Task(void *pvParameters)
{

}

void bme280Init()
{
    if( bme280.init() ) {
        BaseType_t rc;
        rc = xTaskCreate(bme280Task, "bme280", configMINIMAL_STACK_SIZE+1024, NULL, tskIDLE_PRIORITY, NULL);
        if(rc == pdPASS) {
            printlnI("BME280 Task created");
        }
    } else {
        printlnE("BME280 could not be init");
    }
}
