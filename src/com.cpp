
#include <Arduino.h>
#include <freertos/queue.h>

#include "com.h"

Com::Com() {
	m_queue = xQueueCreate(4, sizeof(struct rgbw_t));
}

Com::~Com() {
	if( m_queue ) {
		vQueueDelete(m_queue);
	}
}

bool Com::send(struct rgbw_t *d, TickType_t wait) {
	BaseType_t rc = xQueueSend(m_queue, d, wait);
	return( rc == pdPASS );
}

bool Com::receive(struct rgbw_t *d, TickType_t wait) {
	BaseType_t rc = xQueueReceive(m_queue, (void*)d, wait);
	return( rc == pdPASS );
}
