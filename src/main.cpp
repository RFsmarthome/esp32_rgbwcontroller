#include <Arduino.h>
#include <ArduinoJson.h>

#include <WiFi.h>
#include <WebServer.h>
#include <AutoConnect.h>
#include <ESPmDNS.h>
#include <SPI.h>
#include <SPIFFS.h>


#include <Wire.h>
//#include <AsyncMqttClient.h>

#include <SerialDebug.h>

#include "HTTPUpdateServer.h"

#include "driver/ledc.h"

#include "com.h"
#include "statled.h"
#include "mqtt.h"


#define CHANNELS 1
#define R 0
#define G 1
#define B 2
#define W 3

Com *comQueue;

auto ledChannel = new ledc_channel_t[3][CHANNELS];
auto ledTimer = new ledc_timer_t[3][CHANNELS];
auto ledGpio = new uint8_t[3][CHANNELS];
volatile auto ledGamma = new float[CHANNELS];

const char thingName[] = "esp32";
const char thingApPasswort[] = "start1234";

const int ledPin = 4;
StatLed *statLed;

String mqttPublishBase("rfcentral");
String mqttServer;
String mqttPort;

const char *taskName ="flash";
WebServer webServer;
AutoConnect portal(webServer);
HTTPUpdateServer httpUpdateServer;
AutoConnectAux update("/update", "Update");

static const char AUX_mqtt_setting[] PROGMEM = R"raw(
	[
		{
			"title": "MQTT Settings",
			"uri": "/mqtt",
			"menu": "true",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "MQTT Settings page"
				},
				{
					"name": "mqttserver",
					"type": "ACInput",
					"value": "",
					"label": "Server",
					"placeholder": "MQTT broker server"
				},
				{
					"name": "mqttport",
					"type": "ACInput",
					"value": "",
					"label": "Port",
					"placeholder": "MQTT Port of the broker server"
				},
				{
					"name": "save",
					"type": "ACSubmit",
					"value": "Save",
					"uri": "/mqtt_save"
				}
			]
		},
		{
			"title": "MQTT Settings saved",
			"uri": "/mqtt_save",
			"menu": "false",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "Parameters saved"
				}
			]
		}
	]
)raw";



void getParameters(AutoConnectAux &aux) {
	mqttServer = aux["mqttserver"].value;
	mqttServer.trim();

	mqttPort = aux["mqttport"].value;
	mqttPort.trim();
}

String loadParameters(AutoConnectAux &aux, PageArgument &args)
{
	File param = SPIFFS.open("/param.json", "r");
	if(param) {
		if(aux.loadElement(param)) {
			getParameters(aux);
			printlnA("Parameters loaded");
		} else {
			printlnA("Error by loading parameters");
		}
		param.close();
	} else {
		printlnA("Error by opening parameter file");
	}

	return String("");
}

String saveParameters(AutoConnectAux &aux, PageArgument &args)
{
	AutoConnectAux &mqttSettings = *portal.aux(portal.where());
	getParameters(mqttSettings);

	//AutoConnectInput &mqttserver = mqttSettings["mqttserver"].as<AutoConnectInput>();

	File param = SPIFFS.open("/param.json", "w");
	mqttSettings.saveElement(param, {"mqttserver", "mqttport"});
	param.close();

	return String("");
}

void handleRoot() {
  String  content =
    "<html>"
    "<head>"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    "</head>"
    "<body>"
    "<div class=\"menu\">" AUTOCONNECT_LINK(BAR_32) "</div>"
	"<p>Build git short HASH "
	GIT_SHORT_HASH
	"</p>"
	"<p>Semantic version: "
	GIT_TAG_VERSION
	"</p>"
	"<p>type: esp32_rgbwcontroller</p>"
    "</body>"
    "</html>";

  WebServer &webServer = portal.host();
  webServer.send(200, "text/html", content);
}

void setGamma(uint8_t channel, float gamma) {
	ledGamma[channel] = gamma;
}

uint16_t correctGamma(uint16_t res, float gamma, uint16_t color) {
	uint16_t i = (pow((float)color / (float)res, gamma) * res + 0.5);
	//float c = (float)color / (float)res;
	//uint16_t i = res * powf(c, gamma);
	return i;
}


void initPwm() {
	// Define the channels
	ledChannel[R][0] = LEDC_CHANNEL_0;
	ledChannel[G][0] = LEDC_CHANNEL_1;
	ledChannel[B][0] = LEDC_CHANNEL_2;
	ledChannel[W][0] = LEDC_CHANNEL_3;

	ledTimer[R][0] = LEDC_TIMER_0;
	ledTimer[G][0] = LEDC_TIMER_0;
	ledTimer[B][0] = LEDC_TIMER_1;
	ledTimer[W][0] = LEDC_TIMER_1;

	ledGpio[R][0] = 19;
	ledGpio[G][0] = 18;
	ledGpio[B][0] = 17;
	ledGpio[W][0] = 16;

	ledGamma[0] = 2.8;

	ledc_timer_config_t timerConfig;
	timerConfig.duty_resolution = LEDC_TIMER_8_BIT;
	timerConfig.freq_hz = 1000;
	timerConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
	timerConfig.timer_num = LEDC_TIMER_0;
	ledc_timer_config(&timerConfig);
	timerConfig.timer_num = LEDC_TIMER_1;
	ledc_timer_config(&timerConfig);

	// Setup PWM channels, 1khz 8bit resolution
	ledc_channel_config_t channelConfig;
	channelConfig.duty = correctGamma(255, 2.2f, 50);
	channelConfig.speed_mode = LEDC_HIGH_SPEED_MODE;
	channelConfig.hpoint = 0;
	for(uint8_t l = 0; l < 4; ++l) {
		for(uint8_t c = 0; c < CHANNELS; ++c) {
			channelConfig.channel = ledChannel[l][c];
			channelConfig.gpio_num = ledGpio[l][c];
			channelConfig.timer_sel = ledTimer[l][c];
			ledc_channel_config(&channelConfig);
		}
	}

	ledc_fade_func_install(0);
}

void setChannelRGBW(uint8_t channel, uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
	debugV("Real R: %d, %d", ledChannel[R][channel], correctGamma(255, ledGamma[channel], r));
	debugV("Real G: %d, %d", ledChannel[G][channel], correctGamma(255, ledGamma[channel], g));
	debugV("Real B: %d, %d", ledChannel[B][channel], correctGamma(255, ledGamma[channel], b));
	debugV("Real W: %d, %d", ledChannel[W][channel], correctGamma(255, ledGamma[channel], b));
	ledc_set_duty_and_update(LEDC_HIGH_SPEED_MODE, ledChannel[R][channel], correctGamma(255, ledGamma[channel], r), 0);
	ledc_set_duty_and_update(LEDC_HIGH_SPEED_MODE, ledChannel[G][channel], correctGamma(255, ledGamma[channel], g), 0);
	ledc_set_duty_and_update(LEDC_HIGH_SPEED_MODE, ledChannel[B][channel], correctGamma(255, ledGamma[channel], b), 0);
	ledc_set_duty_and_update(LEDC_HIGH_SPEED_MODE, ledChannel[W][channel], correctGamma(255, ledGamma[channel], b), 0);
}

void taskFlash(void *pvParameters) {
	printlnI("Running flash task");
	for(;;) {
		rgbw_t crgbw;
		bool rc = comQueue->receive(&crgbw, portMAX_DELAY);
		printlnD("flash task wake up");
		if(rc) {
			printlnD("Receive RGB command");
			if(crgbw.channel>=0 && crgbw.channel<CHANNELS) {
				debugV("Set Channel RGB: %d (%d,%d,%d,%d)", crgbw.channel, crgbw.r, crgbw.g, crgbw.b, crgbw.w);
				setChannelRGBW(crgbw.channel, crgbw.r, crgbw.g, crgbw.b, crgbw.w);
				mqttSendRGBW(crgbw.channel, crgbw.r, crgbw.g, crgbw.b, crgbw.w);
			}
		}
	}
}

void setup() {
	delay(1000);
	Serial.begin(115200);

	SPIFFS.begin(true);

	pinMode(ledPin, OUTPUT);
	digitalWrite(ledPin, LOW);

	statLed = new StatLed(GPIO_NUM_4);
	statLed->init();
	statLed->on();

	initPwm();
	setChannelRGBW(0, 50, 0, 0, 0);
	setChannelRGBW(1, 50, 0, 0, 0);

	printlnA("Start rfcentral...");
	WiFi.enableIpV6();
	String macAddress = WiFi.macAddress();
	macAddress.toLowerCase();
	macAddress.replace(":","");
	String hostname = "esp-"+ macAddress;
	debugA("Hostname: %s", hostname.c_str());
	AutoConnectConfig autoConnectConfig; 
	autoConnectConfig.hostName = hostname;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.autoReconnect = true;
	autoConnectConfig.portalTimeout = 60000;
	autoConnectConfig.ticker = false;
	autoConnectConfig.tickerOn = LOW;
	autoConnectConfig.tickerPort = ledPin;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.psk = "start1234";
	autoConnectConfig.autoRise = true;
	autoConnectConfig.retainPortal = true;
	autoConnectConfig.autoReset = true;
	if(portal.load(FPSTR(AUX_mqtt_setting))) {
		printlnA("Load AUX forms");
		AutoConnectAux &mqttSettings = *portal.aux("/mqtt");
		PageArgument args;
		loadParameters(mqttSettings, args);

		portal.on("/mqtt", loadParameters);
		portal.on("/mqtt_save", saveParameters);
	}
	portal.config(autoConnectConfig);
	httpUpdateServer.setup(&webServer);
	portal.join({update});
	if(portal.begin()) {
		debugA("Wifi connected: %s", WiFi.localIP().toString().c_str());
		MDNS.begin(hostname.c_str());
		MDNS.addService("http", "tcp", 80);
	} else {
		printlnA("Timeout for captive portal. Restart ESP");
		ESP.restart();
		delay(1000);
	}

	setChannelRGBW(0, 0, 50, 0, 0);
	setChannelRGBW(1, 0, 50, 0, 0);

	statLed->off();

	comQueue = new Com();
	mqttSetup(hostname, mqttServer, mqttPort, mqttPublishBase, comQueue, statLed);

	WebServer &webServer = portal.host();
	webServer.on("/", handleRoot);

	// Disabled for debuging MQTT
	BaseType_t rc = xTaskCreate(taskFlash, (const char*)"flash", configMINIMAL_STACK_SIZE+4096, NULL, tskIDLE_PRIORITY, NULL);
	if(rc==pdPASS) {
		printlnA("pdPASS by xTaskCreate");
	}
}

void loop() {
	debugHandle();
	portal.handleClient();
}
