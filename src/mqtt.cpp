#include <Arduino.h>

#include <SerialDebug.h>

#include <WiFi.h>
#include <AsyncMqttClient.h>
#include <StringSplitter.h>

#include "com.h"
#include "mqtt.h"
#include "statled.h"
#include "main.h"


WiFiClient wifiClient;

String mqttHostname;
String mqttBasePath;
Com *mqttComQueue;
StatLed *mqttStatLed;

TimerHandle_t mqttTimerHandle;
AsyncMqttClient mqttClient;


void mqttOnConnect(bool sessionPresent) {
	xTimerStop(mqttTimerHandle, 0);
	mqttStatLed->off();
	printlnI("MQTT server connected");

	debugD("Subscribe to: %s", (mqttBasePath + "/cmnd/+/RGBW").c_str());
	mqttClient.subscribe((mqttBasePath + "/cmnd/+/RGBW").c_str(), 1);

	debugD("Subscribe to: %s", (mqttBasePath + "/cmnd/+/GAMMA").c_str());
	mqttClient.subscribe((mqttBasePath + "/cmnd/+/GAMMA").c_str(), 1);
}

void mqttOnDisconnect(AsyncMqttClientDisconnectReason reason) {
	debugI("Disconnect reason: %d", reason);
	printlnI("Start timer to connect to MQTT server");
	mqttStatLed->on();
	xTimerStart(mqttTimerHandle, 0);
}

void mqttOnPublish(uint16_t packetId) {
	debugD("Publish acknowledged (%d)", packetId);
}

void mqttOnMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
	char buffer[128];
	strncpy(buffer, payload, len);
	buffer[len] = 0;

	printlnD("Received message from MQTT");
	debugD("Topic: %s", topic);
	debugD("Payload: %s", buffer);

	rgbw_t crgbw;

	String sTopic(topic);
	StringSplitter *splitter = new StringSplitter(sTopic, '/', 5);
	crgbw.channel = splitter->getItemAtIndex(3).toInt() - 1;

	if(splitter->getItemAtIndex(4).compareTo("GAMMA") == 0) {
		debugD("Gamma Channel: %d", crgbw.channel);
		delete splitter;

		String sPayload(buffer);
		float gamma = sPayload.toFloat();

		debugD("Gamma Value: %f", gamma);
		setGamma(crgbw.channel, gamma);
	} else {
		debugD("RGB Channel: %d", crgbw.channel);
		delete splitter;

		String sPayload(buffer);
		splitter = new StringSplitter(sPayload, ',', 4);
		crgbw.r = splitter->getItemAtIndex(0).toInt();
		crgbw.g = splitter->getItemAtIndex(1).toInt();
		crgbw.b = splitter->getItemAtIndex(2).toInt();
		crgbw.w = splitter->getItemAtIndex(3).toInt();
		delete splitter;

		printlnV("Create RGB packet and send on bus");
		mqttComQueue->send(&crgbw, 200 / portTICK_PERIOD_MS);
	}
}

void mqttTimerCallback()
{
	if(WiFi.isConnected()) {
		printlnI("Connecting to MQTT server...");
		mqttClient.connect();
	}
}


void mqttSetup(String &hostname, String &server, String &port, String &basePath, Com *com, StatLed *statLed) {
	mqttComQueue = com;
	mqttStatLed = statLed;

	String macAddress = WiFi.macAddress();
	macAddress.toLowerCase();
	macAddress.replace(":","");

	mqttBasePath = basePath + "/" + macAddress;

	mqttClient.onDisconnect(mqttOnDisconnect);
	mqttClient.onConnect(mqttOnConnect);
	mqttClient.onPublish(mqttOnPublish);
	mqttClient.onMessage(mqttOnMessage);

	mqttClient.setServer(server.c_str(), port.toInt());

	mqttTimerHandle = xTimerCreate("mqttTimer", pdMS_TO_TICKS(10000), pdTRUE, NULL, reinterpret_cast<TimerCallbackFunction_t>(mqttTimerCallback));
	mqttStatLed->on();
	xTimerStart(mqttTimerHandle, 0);
}

bool mqttSendRGB(uint8_t channel, uint8_t r, uint8_t g, uint8_t b)
{
	String topic = mqttBasePath + "/stat/" + String(channel) + "/RGB";
	String payload = String(r) + "," + String(g) + "," + String(b);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}

bool mqttSendRGBW(uint8_t channel, uint8_t r, uint8_t g, uint8_t b, uint8_t w)
{
	String topic = mqttBasePath + "/stat/" + String(channel) + "/RGBW";
	String payload = String(r) + "," + String(g) + "," + String(b) + "," + String(w);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}

bool mqttSendTemp(uint8_t channel, float temp)
{
	String topic = mqttBasePath + "/stat/" + String(channel) + "/TEMP";
	String payload = String(temp);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}

bool mqttSendHumidity(uint8_t channel, float hum)
{
	String topic = mqttBasePath + "/stat/" + String(channel) + "/HUMIDITY";
	String payload = String(hum);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}

bool mqttSendPressure(uint8_t channel, float press)
{
	String topic = mqttBasePath + "/stat/" + String(channel) + "/PRESSURE";
	String payload = String(press);
	debugV("Publish on %s with value %s", topic.c_str(), payload.c_str());
	uint16_t rc = mqttClient.publish(topic.c_str(), 1, false, payload.c_str(), payload.length());
	return(rc>0);
}
