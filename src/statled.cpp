#include "statled.h"

#include <driver/gpio.h>

StatLed::StatLed(gpio_num_t gpio) : m_gpio(gpio)
{

}

void StatLed::init()
{
	gpio_config_t conf;
	conf.intr_type = GPIO_INTR_DISABLE;
	conf.mode = GPIO_MODE_OUTPUT;
	conf.pin_bit_mask = (1ULL<<m_gpio);
	conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
	conf.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&conf);
}

void StatLed::on()
{
	gpio_set_level(m_gpio, 0);
}

void StatLed::off()
{
	gpio_set_level(m_gpio, 1);
}

void StatLed::set(bool b)
{
	if(b) {
		gpio_set_level(m_gpio, 0);
	} else {
		gpio_set_level(m_gpio, 1);
	}
}
